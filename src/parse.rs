use cpython::{PyResult, Python, PyObject, ObjectProtocol, PyErr, exc};


py_module_initializer!(fastdtparse, initfastdtparse, PyInit_fastdtparse, |py, m| {
    try!(m.add(py, "__doc__", "This module is implemented in Rust."));
    try!(m.add(py, "parse_isoformat", py_fn!(py, parse_isoformat_py(datestr: &str))));
    Ok(())
});


fn parse_isoformat(datestr: &str) -> Result<(i32, u8, u8, u8, u8, u8), &'static str> {
    if datestr.len() < 19 {
        return Err("string is too short");
    }
    let year_str = &datestr[..4];
    let month_str = &datestr[5..7];
    let day_str = &datestr[8..10];
    let hour_str = &datestr[11..13];
    let minute_str = &datestr[14..16];
    let second_str = &datestr[17..19];
    let year = year_str.parse().map_err(|_| "year is not integer")?;
    let month = month_str.parse().map_err(|_| "month is not integer")?;
    let day = day_str.parse().map_err(|_| "day is not integer")?;
    let hour = hour_str.parse().map_err(|_| "hour is not integer")?;
    let minute = minute_str.parse().map_err(|_| "minute is not integer")?;
    let second = second_str.parse().map_err(|_| "second is not integer")?;
    Ok((year, month, day, hour, minute, second))
}


pub fn parse_isoformat_py(py: Python, datestr: &str) -> PyResult<PyObject> {
    let dt_mod = try!(py.import("datetime"));
    let dt_dt = try!(dt_mod.get(py, "datetime"));
    let args = try!(parse_isoformat(datestr).map_err(|e| PyErr::new::<exc::ValueError, &str>(py, e)));
    try!(dt_dt.call(py, args, None)).extract(py)
}
